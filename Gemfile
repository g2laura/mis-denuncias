source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.0.0'

# Use mysql as the database for Active Record
gem 'mysql2'

# Use SCSS for stylesheets
gem 'sass-rails', '~> 4.0.0'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# Authentication
gem 'devise'

# Authorization
gem 'cancan'

# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.0.0'

# Bootstrap 3 Integration
gem 'therubyracer', platforms: :ruby
gem 'less-rails'
gem 'twitter-bootstrap3-rails'

# Use jquery as the JavaScript library
gem 'jquery-rails'
gem 'jquery-ui-rails'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 1.2'

group :doc do
  # bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', require: false
end

# Eywa (don't require it since only scripts are used)
gem 'eywa', git: 'git@git.divux.com:divux/eywa.git', require: false

# Use unicorn as the app server
gem 'unicorn'

# Use Capistrano for deployment
group :development do
  gem 'capistrano',         '~> 3',   require: false
  gem 'capistrano-rails',   '~> 1.1', require: false
  gem 'capistrano-bundler', '~> 1.1', require: false
  gem 'capistrano-rbenv',   '~> 2.0', require: false
end

# File Upload
gem 'carrierwave'

# Video Converter
gem 'carrierwave-video'

# jQuery timeago plugin
gem 'rails-timeago'

# Pagination
gem 'kaminari'

# Google maps for rails
gem 'gmaps4rails'

# Localization
gem "geocoder"

# Taggable
gem 'acts-as-taggable-on'

# Twitter
gem 'twitter', '5.7.1'
gem 'twitter-text'

# Delayed Job
gem 'delayed_job_active_record'

# Local time
gem 'local_time'
