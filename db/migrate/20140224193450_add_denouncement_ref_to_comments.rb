class AddDenouncementRefToComments < ActiveRecord::Migration
  def change
    add_reference :comments, :denouncement, index: true
  end
end
