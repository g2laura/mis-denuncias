class RemoveTagIdFromDenouncements < ActiveRecord::Migration
  def change
    remove_column :denouncements, :tag_id, :integer
  end
end
