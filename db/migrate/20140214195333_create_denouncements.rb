class CreateDenouncements < ActiveRecord::Migration
  def change
    create_table :denouncements do |t|
      t.string :title
      t.text :description
      t.string :via
      t.references :country, index: true
      t.references :category, index: true
      t.string :status
      t.string :image
      t.string :video
      t.text :location
      t.references :tag, index: true
      t.references :user, index: true

      t.timestamps
    end
  end
end
