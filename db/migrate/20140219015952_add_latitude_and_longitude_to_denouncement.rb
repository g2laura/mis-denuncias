class AddLatitudeAndLongitudeToDenouncement < ActiveRecord::Migration
  def change
    add_column :denouncements, :latitude, :float
    add_column :denouncements, :longitude, :float
  end
end
