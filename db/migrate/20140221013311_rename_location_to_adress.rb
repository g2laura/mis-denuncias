class RenameLocationToAdress < ActiveRecord::Migration
  def change
    rename_column :denouncements, :location, :address
  end
end
