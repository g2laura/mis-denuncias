class AddTweetedToDenouncements < ActiveRecord::Migration
  def change
    add_column :denouncements, :tweeted, :boolean
  end
end
