categories = [
  'parking',
  'pollution',
  'theft',
  'traffic',
  'other',
  'supply'
]

countries = [
  'Venezuela',
  'Panama',
  'Italia'
]

categories.each do |name|
  Category.where(name: name).first_or_create
end

countries.each do |name|
  Country.where(name: name).first_or_create
end

User.where(email: 'admin@local.host').first_or_create do |user|
  user.password = '12345678'
  user.role = 'admin'
end
