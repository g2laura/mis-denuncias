class CommentsController < ApplicationController
  before_action :set_comment, only: [:show, :edit, :update, :destroy]
  before_action :set_denouncement, only: [:index, :new, :create]

  # GET /comments/new
  def new
    @comment = Comment.new(denouncement: @denouncement)
  end

  # POST /comments
  # POST /comments.json
  def create
    @comment = Comment.new(comment_params)
    @comment.denouncement = @denouncement

    respond_to do |format|
      if @comment.save
        format.html { redirect_to @denouncement, notice: t('comment.created') }
      else
        format.html { render action: 'new' }
      end
    end
  end

  # DELETE /comments/1
  # DELETE /comments/1.json
  def destroy
    @denouncement = @comment.denouncement
    @comment.destroy
    respond_to do |format|
      format.html { redirect_to @denouncement }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_comment
      @comment = Comment.find(params[:id])
    end

    def set_denouncement
      @denouncement = Denouncement.find(params[:denouncement_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def comment_params
      params.require(:comment).permit(:title, :content)
    end
end
