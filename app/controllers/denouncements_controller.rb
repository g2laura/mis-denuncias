class DenouncementsController < ApplicationController
  before_action :set_denouncement, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource except: [:create]

  # GET /denouncements
  # GET /denouncements.json
  def index
    @denouncement = Denouncement.new
    @tags = ActsAsTaggableOn::Tag.select('DISTINCT tags.name').map{ |x| x.name}
    @tweets = Denouncement.last_tweets
    Denouncement.delay.search_for_mentions

    if params[:q]
      @denouncements = Denouncement.search(params[:q], params[:page])
    elsif params[:c]
      if params[:c] == Category::PARKING
        @denouncements = Denouncement.parking.page params[:page]
      elsif params[:c] == Category::POLLUTION
        @denouncements = Denouncement.pollution.page params[:page]
      elsif params[:c] == Category::THEFT
        @denouncements = Denouncement.theft.page params[:page]
      elsif params[:c] == Category::TRAFFIC
        @denouncements = Denouncement.traffic.page params[:page]
      elsif params[:c] == Category::SUPPLY
        @denouncements = Denouncement.supply.page params[:page]
      elsif params[:c] == Category::OTHER
        @denouncements = Denouncement.other.page params[:page]
      end
    else
      @denouncements = Denouncement.all.order(created_at: :desc).page params[:page]
    end
  end

  # GET /denouncements/1
  # GET /denouncements/1.json
  def show
    @comments = @denouncement.comments.order(:created_at).reverse_order
  end

  # GET /denouncements/new
  def new
    @denouncement = Denouncement.new
  end

  # GET /denouncements/1/edit
  def edit
    @comments = @denouncement.comments.order(:created_at).reverse_order
  end

  # POST /denouncements
  # POST /denouncements.json
  def create
    @denouncement        = Denouncement.new(denouncement_params)
    @denouncement.status = Denouncement::OPEN
    @denouncement.via    = Denouncement::WEB

    respond_to do |format|
      if @denouncement.save
        format.html { redirect_to @denouncement, notice: t('denouncement.created') }
        format.json { render action: 'show', status: :created, location: @denouncement }
      else
        format.html { render action: 'new' }
        format.json { render json: @denouncement.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /denouncements/1
  # PATCH/PUT /denouncements/1.json
  def update
    respond_to do |format|
      if @denouncement.update(denouncement_params)
        format.html { redirect_to @denouncement, notice: t('denouncement.updated') }
      else
        format.html { render action: 'edit' }
        format.json { render json: @denouncement.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /denouncements/1
  # DELETE /denouncements/1.json
  def destroy
    @denouncement.destroy
    respond_to do |format|
      format.html { redirect_to denouncements_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_denouncement
      @denouncement = Denouncement.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def denouncement_params
      params.require(:denouncement).permit(:title, :description, :via, :country_id, :category_id, :status, 
      :image, :video, :address, :latitude, :longitude, :tag_list, :user_id)
    end
end
