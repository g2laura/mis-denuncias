// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery.ui.all
//= require jquery_ujs
//= require gmaps/google
//= require twitter/bootstrap
//= require local_time
//= require_tree .

$(function () {
  $('.select_category').click(function() {
    $('.category-form img').removeClass('active');
    $(this).addClass('active');
    $('input[id=denouncement_category_id]').val($(this).attr('id'));
  });

  $('.graph img').each(function() {
    var count = $(this).data("count");
    var total = $(this).data("total");
    var percentage = (count/total)*100;
    if(percentage < 5) {
      percentage += 5;
    }
    if(percentage == 100) {
      percentage = 91.5;
    }
    $(this).css("left", percentage + "%");
  });

  $('#show-comments').click(function(){
    $('.comments-list .show-button-box').hide();
    $('.comments-list .comment-content').show();
    $('.comments-list .hide-button-box').show();
  });

  $('#hide-comments').click(function(){
    $('.comments-list .show-button-box').show();
    $('.comments-list .comment-content:gt('+($('.comments-list .comment-content').length-2)+')').hide();
    $('.comments-list .hide-button-box').hide();
  });
})