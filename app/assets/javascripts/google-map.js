$(function () {
  var geocoder = new google.maps.Geocoder();
  var myLatlng = new google.maps.LatLng(10.4683918, -66.8903658);
  var map = new google.maps.Map(document.getElementById('map-canvas'), {
    zoom: 17,
    tilt: 0
  });

  var marker = new google.maps.Marker({
    position: myLatlng,
    map: map,
    title: "Ubicación",
    draggable: true
  });

  function updateMarkerPosition(latLng) {
    $('#denouncement_latitude').val(latLng.lat());
    $('#denouncement_longitude').val(latLng.lng());
  }

  function initialize() {
    // Try HTML5 geolocation
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function (position) {
        var pos = new google.maps.LatLng(position.coords.latitude,
                                         position.coords.longitude);

        marker.setPosition(pos);

        updateMarkerPosition(marker.getPosition());
        map.setCenter(pos);
      }, function() {
        marker.setPosition(myLatlng);
        map.setCenter(myLatlng);
      });
    } else {
      marker.setPosition(myLatlng);
      map.setCenter(myLatlng);
    }

    google.maps.event.addListener(marker, 'drag', function(event) {
      console.log('called drag');
      updateMarkerPosition(event.latLng);
    });

    google.maps.event.addListener(map, 'click', function(event) {
      if (marker.getDraggable()) {
        updateMarkerPosition(event.latLng);
        marker.setPosition(event.latLng);
      }
    });
  }

  // Onload handler to fire off the app.
  google.maps.event.addDomListener(window, 'load', initialize);
})