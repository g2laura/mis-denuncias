jQuery ->
  $('#denouncement_tag_list').each ->
    tags = $(this).data('tags')
    $(this).tagit availableTags: tags

  $('#map').each ->
    latitude  = $(this).data('latitude')
    longitude = $(this).data('longitude')

    latLng = new google.maps.LatLng(latitude, longitude)
    mapOptions = zoom: 17, center: latLng

    map = new google.maps.Map this, mapOptions
    marker = new google.maps.Marker position: latLng, map: map
