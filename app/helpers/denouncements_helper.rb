module DenouncementsHelper
  include Twitter::Autolink
  def status_for_select(actual_status)
    kinds = [ Denouncement::OPEN, Denouncement::IN_PROGRESS, Denouncement::SCALING, Denouncement::REJECTED, Denouncement::CLOSED ]
    translations = kinds.map { |status| I18n.translate "denouncement.status.#{status}" }

    options_for_select(translations.zip(kinds), actual_status)
  end
end
