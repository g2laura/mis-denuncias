json.array!(@denouncements) do |denouncement|
  json.extract! denouncement, :id, :title, :description, :via, :country_id, :category_id, :status, :image, :video, :location, :tag_id, :user_id
  json.url denouncement_url(denouncement, format: :json)
end
