class Denouncement < ActiveRecord::Base
  require "twitter"

  belongs_to :country
  belongs_to :category
  belongs_to :user
  has_many   :comments

  mount_uploader :image, ImageUploader
  mount_uploader :video, VideoUploader

  geocoded_by :address
  after_validation :geocode

  acts_as_ordered_taggable

  OPEN        = "open"
  IN_PROGRESS = "in_progress"
  SCALING     = "scaling"
  REJECTED    = "rejected"
  CLOSED      = "closed"

  WEB         = "web"
  TWITTER     = "twitter"
  SMS         = "sms"

  CLIENT = Twitter::REST::Client.new do |config|
    config.consumer_key        = "HyA83JZ04Wh7xONEMfCfbQ"
    config.consumer_secret     = "UJn6bSEKmDpcmS5yG8W7NO1xCYzMjMkOPSjdoiAMk"
    config.access_token        = "161024069-kiuq1eJPgJxnlqlNNONm9mETJQTfOsStnQoM8kXV"
    config.access_token_secret = "HUt6QpvxwdKQOpqJmbDkbSLcmMVNwanCDZQs4RHXA"
  end

  scope :parking,   -> { joins(:category).where('categories.name = ?', Category::PARKING).order(created_at: :desc) }
  scope :pollution, -> { joins(:category).where('categories.name = ?', Category::POLLUTION).order(created_at: :desc) }
  scope :theft,     -> { joins(:category).where('categories.name = ?', Category::THEFT).order(created_at: :desc) }
  scope :traffic,   -> { joins(:category).where('categories.name = ?', Category::TRAFFIC).order(created_at: :desc) }
  scope :other,     -> { joins(:category).where('categories.name = ?', Category::OTHER).order(created_at: :desc) }
  scope :supply,    -> { joins(:category).where('categories.name = ?', Category::SUPPLY).order(created_at: :desc) }

  def self.search(search, page_number)
    if search
      Denouncement.where('lower(title) like ? or lower(description) like ?',
      "%#{search.downcase}%",
      "%#{search.downcase}%").order(created_at: :desc).page page_number
    end
  end

  def self.last_tweets
    return Denouncement::CLIENT.user_timeline(count: 3)
  end

  def self.search_for_mentions
    Denouncement::CLIENT.mentions_timeline(count: 15).each do |mention|
      denouncement = Denouncement.where(title: mention.text).first_or_create do |denouncement|
        denouncement.created_at  = mention.created_at
        denouncement.status      = Denouncement::OPEN
        denouncement.via         = Denouncement::TWITTER
        denouncement.description = mention.text
        denouncement.category    = Category.where(name: Category::OTHER).first
        denouncement.image       = mention.attrs[:entities][:media].try(:first).try(:[], :media_url)
        denouncement.latitude    = mention.geo.coordinates.try(:first)
        denouncement.longitude   = mention.geo.coordinates.try(:last)
        denouncement.tweeted     = false
      end

      unless denouncement.tweeted
        Denouncement::CLIENT.update!(denouncement.title + "(vía @" + mention.user.screen_name + ")")
        denouncement.update tweeted: true
      end
    end
  end
end

