class Category < ActiveRecord::Base
  has_many :denouncements

  PARKING   = 'parking'
  POLLUTION = 'pollution'
  THEFT     = 'theft'
  TRAFFIC   = 'traffic'
  OTHER     = 'other'
  SUPPLY    = 'supply'

  def translated_category
    I18n.t(name, :scope => 'category.kinds')
  end

end
