Complaint::Application.routes.draw do
  root to: "denouncements#index"

  resources :denouncements do
    resources :comments, shallow: true
  end

  resources :countries

  resources :categories

  devise_for :users
end
