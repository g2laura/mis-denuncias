# config valid only for Capistrano 3.1
lock '3.1.0'

set :application, 'misdenuncias'
set :repo_url, 'git@git.divux.com:mis-denuncias/ror.git'

set :deploy_to, "/srv/apps/saas/#{fetch(:application)}"

set :log_level, :debug

set :linked_files, %w{config/database.yml config/unicorn.rb}
set :linked_dirs,  %w{log tmp/pids tmp/cache tmp/sockets public/system public/uploads}

set :bundle_binstubs, nil

set :rbenv_type, :user
set :rbenv_ruby, '2.0.0-p195'
set :rbenv_gemsets, 'mis-denuncias-web'

set :default_env, { path: "~/.rbenv/shims:~/.rbenv/bin:$PATH" }

set :nginx_config, 'nginx.conf.erb'
set :nginx_vhost,  'misdenuncias.saas4.us'

after 'deploy:publishing', 'unicorn:restart'
